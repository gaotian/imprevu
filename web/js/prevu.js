function displayTables(show){

    $('table').hide();

    $('#notices tr').each(
        function(index){
            console.log('show ='+show);
            if (show !=true){
                if( index > 10){
                    $(this).hide();
                }
            }
            else{
                $(this).fadeIn();
                $(".showall").hide();
            }
        }
    );

    $('table').fadeIn(1000);
}

$('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
    }
);


$(document).ready(function()
    {
        $(".tablesorter").tablesorter();
        $('select').material_select();
        $('textarea').trigger('autoresize');
        $('.parallax').parallax();
        $('.datepicker').pickadate({
            //selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });

    }
);
//
// $("body").hide();
// $("body").fadeIn(1000);

