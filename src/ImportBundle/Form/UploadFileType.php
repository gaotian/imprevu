<?php

namespace ImportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class UploadFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('attachment', FileType::class, array(
			'attr' =>array(
				'class' =>'filestyle',
				'data-icon' =>'false',
				'data-buttonText'=>'Parcourir',
				'data-buttonName'=>'btn-primary btn-dark',
				)
			))
            ->add('cancel', ButtonType::class, array(
                'attr' => array(
                    'class' => 'modal-action modal-close btn-flat'
                ),
                'label' => 'Annuler',
            ))

			->add('upload', SubmitType::class, array(
				'attr' => array(
				    'class' => 'btn-flat'
                    ),
				'label' => 'Importer',
			));

    }

    public function getName()
    {
        return 'uploadfile';
    }
}
