<?php

namespace ImportBundle\Controller;

use AppBundle\Entity\Statistique;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class StatsController extends Controller
{
    /**
     * @Route("/import/stats", name="import_stats")
     */
    public function ImportStatsAction(){

        $this->import(1);
        $this->import(2);
        $this->import(3);
        return $this->redirectToRoute('import');

    }

    public function import($library){

        switch ($library){
            case 1:
                $title = "up8";
                break;
            case 2:
                $title = "rbx";
                break;
            case 3:
                $title = "scl";
                break;
        }

        $em = $this->getDoctrine()->getManager();

        $totalUp8 = $em->getRepository('AppBundle:Book')->totalBookFor1library($library);
        $borrowersUp8 = $em->getRepository("AppBundle:Borrower")->totalBorrowerFor1library($library);
        $itemsUp8 = $em->getRepository("AppBundle:Item")->totalItemFor1library($library);
        $issuesUp8 = $em->getRepository("AppBundle:Issue")->totalIssueFor1library($library);

        $repository = $em->getRepository('AppBundle:Statistique');
        $stat = $repository->findOneByTitle($title);

        $now = new \DateTime();

        if(count($stat)<1){
            $stat = new Statistique();
            $stat->setDateCreation($now);
        }

        $stat->setTitle($title);
        $stat->setBooks($totalUp8['nb']);
        $stat->setBorrowers($borrowersUp8['nb']);
        $stat->setItems($itemsUp8['nb']);
        $stat->setIssues($issuesUp8['nb']);
        $stat->setLastUpdate($now);

//        dump($now);die;

        $this->addFlash('success', 'Statistiques '.$title.' mis à jour');
        $em->persist($stat);
        $em->flush();

    }

}

