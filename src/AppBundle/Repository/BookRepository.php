<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class BookRepository extends EntityRepository
{
    public function findAllOrderdByTitle()
    {
        return $this->createQueryBuilder('book')
            ->setMaxResults(100)
            ->getQuery()
            ->execute();
    }

    //Nombre total de livres dans Prévu
    public function totalBooks(){

        return $this->getEntityManager()->createQuery("SELECT COUNT(b) as nb FROM AppBundle:Book b")
            ->getSingleResult();
//            ->execute();
    }

    //Nombre total de livres pour une bibliothèque (plus performant qu'en comptant les books)
    public function totalBookFor1library($library){
        return $this->getEntityManager()->createQuery("SELECT COUNT(DISTINCT(b.prevu)) as nb FROM AppBundle:Key b JOIN b.library l WHERE l.idlibrary = :library")
            ->setParameter('library', $library)
            ->getSingleResult();
    }

    //Liste des doublons
    public function doublons(){
        return $this->getEntityManager()->createQuery("SELECT   COUNT(*) AS nbr_doublon, title, author, isbn FROM book GROUP BY title, author, isbn HAVING COUNT(*) > 1");
    }

}



