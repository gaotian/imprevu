<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Created by PhpStorm.
 * User: gaetan
 * Date: 05/04/2017
 * Time: 11:13
 */


/**
 * TempBNEmprunteurs
 *
 * @ORM\Table(name="tempBNEmprunteurs")
 * @ORM\Entity()
 */
class TempBNEmprunteurs
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cb", type="string", length=45, nullable=true)
     */
    private $cb;

    /**
     * @var string
     *
     * @ORM\Column(name="localisation", type="string", length=45, nullable=true)
     */
    private $localisation;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="birthday", type="integer", length=4)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=45, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="csp", type="string", length=45, nullable=true)
     */
    private $csp;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="integer")
     */
    private $trimestre;

    /**
     * @ORM\Column(type="integer")
     */
    private $janvier2017;

    /**
     * @ORM\Column(type="integer")
     */
    private $fevrier2017;

    /**
     * @ORM\Column(type="integer")
     */
    private $mars2017;
}

//Import des fichiers csv
//cb,localisation,type,birthday,gender,csp,adresse,trimestre,janvier2017, fevrier2017, mars2017