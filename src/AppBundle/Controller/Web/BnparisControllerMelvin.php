<?php
/**
 * Created by PhpStorm.
 * User: melvi
 * Date: 16/05/2017
 * Time: 11:46
 */

namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class BnparisControllerMelvin extends Controller
{

    /**
     * @Route("/paris/bn/books/top", name="paris_bn_top_books")
     */
    public function bnTopBooks()
    {

        $em = $this->getDoctrine()->getManager();

        //------- top books
        $query = $em->createQuery("SELECT COUNT(s.ean) as nb, s.title as title, s.ean as ean, s.author as author, s.image as image FROM AppBundle:StatsBNLogs s GROUP BY s.ean ORDER BY nb DESC")->setMaxResults(100);
        $top_books = $query->getResult();

        return $this->render('AppBundle:Bnparis:top_books.html.twig', array(
            'top_books' => $top_books
        ));

    }


    /**
     * @Route("/paris/bn/authors/top", name="paris_bn_top_authors")
     */
    public function bnTopAuthors()
    {

        $em = $this->getDoctrine()->getManager();

        //------- top books
        $query = $em->createQuery("SELECT COUNT(s.author)as nb, s.author as author FROM AppBundle:StatsBNLogs s GROUP BY s.author ORDER BY nb DESC")->setMaxResults(50);
        $top_authors = $query->getResult();

        return $this->render('AppBundle:Bnparis:top_authors.html.twig', array(
            'top_authors' => $top_authors
        ));

    }



}