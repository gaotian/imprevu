<?php
/**
 * Created by PhpStorm.
 * User: Ale PDERY
 * Date: 16/05/2017
 * Time: 11:59
 */


namespace AppBundle\Controller\Web;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class BnparisControllerAle extends Controller
{
    /**
     * @Route("/paris/bn/classification/{classification}", name="paris_bn_classification")
     */
    public function classificationAction($classification){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT COUNT(s) as value ,s.classification as key FROM AppBundle:StatsBNLogs s WHERE s.classification != '' GROUP by key ORDER BY value DESC")->setMaxResults(10);
        $totalClassif = $query->getResult();


        //------- top books
        $query = $em->createQuery("SELECT COUNT(s.ean) as nb, s.title as title, s.ean as ean, s.author as author FROM AppBundle:StatsBNlogs s WHERE s.classification = :classification GROUP BY s.ean ORDER BY nb DESC");
        $query->setParameter('classification', $classification);
        $query->setMaxResults(8);
        $top_books = $query->getResult();


        //------- top 5 authors
        $query = $em->createQuery("SELECT COUNT(s.author)as nb, s.author as author FROM AppBundle:StatsBNlogs s WHERE s.classification = :classification GROUP BY s.author ORDER BY nb DESC");
        $query->setParameter('classification', $classification);
        $query->setMaxResults(8);
        $top_authors = $query->getResult();

        return $this->render('AppBundle:Bnparis:classification.html.twig', array(
            'totalClassif' => $totalClassif,
            'top_books' => $top_books,
            'top_authors' => $top_authors,
            'classification' => $classification
        ));
    }
}
